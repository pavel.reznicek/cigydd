#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def rotate_list(l, count):
    u"""Turns the given list by given item count.
    It’s no mirroring but rotation,
    while the list is taken as a closed cycle
    vhich undergoes a turn (rotation).
"""
    if not isinstance(l, list):
        l = list(l)
    count = count % len(l)
    if count == 0:
        return l[:]
    elif count > 0:
        return l[-count:] + l[:-count]
    elif count < 0:
        return l[:-count] + l[-count:]
    