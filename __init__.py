#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__all__=(
  "Pyp",
  "TestDialog",
  "bin",
  "ffdir",
  "graphics",
  "irange",
  "lists",
  "listtext",
  "properties"
)

import os

def printdir(item):
  for subitem in dir(item):
    print(subitem)

def dirtext(item, separator = os.linesep):
  result = separator.join(dir(item))
  return result

try:
  import wx
except ImportError:
  wx = None
  print("VAROVÁNÍ: Nepodařilo se importovat knihovnu wx, zřejmě nám tu chybí!")

if wx:
  class TestDialog(wx.Dialog):
    def __init__(self):
      wx.Dialog.__init__(self,None)
      szr=self.szr=wx.BoxSizer(wx.VERTICAL)
      self.SetSizer(szr)
      self.Show()

def Pyp():
  import os
  import os.path as p
  from cigydd import player
  #import cigydd as mo
  #d = p.dirname(mo.__file__)
  d = p.dirname(__file__)
  wave = p.join(d, "pyp", "pyp.wav")
  if wx and wx.Platform == '__WXMSW__':
    if hasattr(wx, 'Sound'):
      snd=wx.Sound(wave)
      snd.Play()
    elif hasattr(wx, 'adv'):
      snd = wx.adv.Sound(wave)
      snd.Play()
    else:
      snd = player.OneTimeMediaPlayer(wave)
  else:
    os.system('aplay %s 2>/dev/null &'%wave)
    #snd=wx.Sound(wave)
    #snd.Play()
    #wx.Sound_PlaySound(wave)
  
if __name__=='__main__':
  if wx:
    app=wx.App()
  Pyp()
  if wx:
    app.MainLoop()


