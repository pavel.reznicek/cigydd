﻿#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
ff.py – scriptus convertens peliculam fonticam
          in peliculam
              codicibus datis incoditam
              in formato coniuncto
              cum sex centorum et quadragintorum (DCXL) pixelorum
                  imaginis latitudine maximali

Usus:
ff.py [-v <codex videalis>] [-a <codex audialis>] <folium fonticum>
"""

import os
import sys
import argparse
import ffmpeg
from ffmpeg._probe import Error as ProbaeError

FSENC = sys.getfilesystemencoding()

CODEX_XVID      = "libxvid"
CODEX_FFV1      = "ffv1"
CODEX_MPEG2     = "mpeg2video"
CODEX_MPEG4     = "mpeg4"
CODEX_THEORA    = "libtheora"
CODEX_X264      = "libx264"
CODEX_X265      = "hevc"

CODEX_MP3       = "libmp3lame"
CODEX_MP2       = "mp2"
CODEX_VORBIS    = "libvorbis"
CODEX_OPUS      = "libopus"
CODEX_SPEEX     = "speex"
CODEX_FLAC      = "flac"
CODEX_AAC       = "aac"

NOMINA_CODICUM_VIDEALIUM = [
    CODEX_XVID,
    CODEX_FFV1,
    CODEX_MPEG2,
    CODEX_MPEG4,
    CODEX_THEORA,
    CODEX_X264,
    CODEX_X265,
    None
]

NOMINA_CODICUM_AUDIALIUM = [
    CODEX_MP3,
    CODEX_MP2,
    CODEX_VORBIS,
    CODEX_OPUS,
    CODEX_SPEEX,
    CODEX_FLAC,
    CODEX_AAC,
    None
]

FORMATUS_AVI    = "avi"
FORMATUS_MPEG   = "mpeg"
FORMATUS_OGV    = "ogv"
FORMATUS_MP4    = "mp4"
FORMATUS_MKV    = "mkv"

FORMATUS_MP3    = "mp3"
FORMATUS_MP2    = "mp2"
FORMATUS_OGG    = "ogg"
FORMATUS_OPUS   = "opus"
FORMATUS_FLAC   = "flac"
FORMATUS_M4A    = "m4a"

NOMINA_FORMATORUM_VIDEALIUM = [
    FORMATUS_MKV,
    FORMATUS_MP4,
    FORMATUS_AVI,
    FORMATUS_MPEG,
    FORMATUS_OGV
]
NOMINA_FORMATORUM_AUDIALIUM = [
    FORMATUS_M4A,
    FORMATUS_MP3,
    FORMATUS_MP2,
    FORMATUS_OGG,
    FORMATUS_OPUS,
    FORMATUS_FLAC,
]

CLAVIS_CODICUM_VIDEALIUM = "codices videales"
CLAVIS_CODICUM_AUDIALIUM = "codices audiales"

CODICES_FORMATORUM = {
    FORMATUS_AVI: {
        CLAVIS_CODICUM_VIDEALIUM:
            NOMINA_CODICUM_VIDEALIUM,
        CLAVIS_CODICUM_AUDIALIUM:
            NOMINA_CODICUM_AUDIALIUM + [None]
    },
    FORMATUS_MPEG: {
        CLAVIS_CODICUM_VIDEALIUM: [
            CODEX_MPEG2
        ],
        CLAVIS_CODICUM_AUDIALIUM: [
            CODEX_MP2,
            None
        ]
    },
    FORMATUS_OGV: {
        CLAVIS_CODICUM_VIDEALIUM: [
            CODEX_FFV1,
            CODEX_THEORA,
        ],
        CLAVIS_CODICUM_AUDIALIUM: [
            CODEX_VORBIS,
            CODEX_OPUS,
            CODEX_SPEEX,
            CODEX_FLAC,
            None
        ]
    },
    FORMATUS_MP4: {
        CLAVIS_CODICUM_VIDEALIUM: [
            CODEX_X264,
            CODEX_X265
        ],
        CLAVIS_CODICUM_AUDIALIUM: [
            CODEX_MP3,
            CODEX_AAC,
            None
        ]
    },
    FORMATUS_MKV: {
        CLAVIS_CODICUM_VIDEALIUM:
            NOMINA_CODICUM_VIDEALIUM,
        CLAVIS_CODICUM_AUDIALIUM:
            NOMINA_CODICUM_AUDIALIUM + [None]
    },
    FORMATUS_MP3: {
        CLAVIS_CODICUM_VIDEALIUM: [
            None
        ],
        CLAVIS_CODICUM_AUDIALIUM: [
            CODEX_MP3
        ]
    },
    FORMATUS_MP2: {
        CLAVIS_CODICUM_VIDEALIUM: [
            None
        ],
        CLAVIS_CODICUM_AUDIALIUM: [
            CODEX_MP2
        ]
    },
    FORMATUS_OGG: {
        CLAVIS_CODICUM_VIDEALIUM: [
            None
        ],
        CLAVIS_CODICUM_AUDIALIUM: [
            CODEX_VORBIS,
            CODEX_OPUS,
            CODEX_SPEEX,
            CODEX_FLAC
        ]
    },
    FORMATUS_OPUS: {
        CLAVIS_CODICUM_VIDEALIUM: [
            None
        ],
        CLAVIS_CODICUM_AUDIALIUM: [
            CODEX_OPUS
        ]
    },
    FORMATUS_FLAC: {
        CLAVIS_CODICUM_VIDEALIUM: [
            None
        ],
        CLAVIS_CODICUM_AUDIALIUM: [
            CODEX_FLAC
        ]
    },
    FORMATUS_M4A: {
        CLAVIS_CODICUM_VIDEALIUM: [
            None
        ],
        CLAVIS_CODICUM_AUDIALIUM: [
            CODEX_AAC
        ]
    }
}

CLAVIS_CODICIS = "codex"
CLAVIS_RATIONIS_BITORUM = "ratio bitorum"
CLAVIS_IMAGINUM_PER_SECUNDAM = "imagines per secundam"

CODICES = {
    CODEX_XVID: {
        CLAVIS_CODICIS: "libxvid",
        CLAVIS_RATIONIS_BITORUM: 2048,
        CLAVIS_IMAGINUM_PER_SECUNDAM: 25
    },
    CODEX_MPEG2: {
        CLAVIS_CODICIS: "mpeg2video",
        CLAVIS_RATIONIS_BITORUM: 2048,
        CLAVIS_IMAGINUM_PER_SECUNDAM: 25
    },
    CODEX_MPEG4: {
        CLAVIS_CODICIS: "mpeg4",
        CLAVIS_RATIONIS_BITORUM: 2048,
        CLAVIS_IMAGINUM_PER_SECUNDAM: 25
    },
    CODEX_THEORA: {
        CLAVIS_CODICIS: "libtheora",
        CLAVIS_RATIONIS_BITORUM: 2048,
        CLAVIS_IMAGINUM_PER_SECUNDAM: 25
    },
    CODEX_X264: {
        CLAVIS_CODICIS: "libx264",
        CLAVIS_RATIONIS_BITORUM: 0,
        CLAVIS_IMAGINUM_PER_SECUNDAM: 25
    },
    CODEX_X265: {
        CLAVIS_CODICIS: "hevc",
        CLAVIS_RATIONIS_BITORUM: 0,
        CLAVIS_IMAGINUM_PER_SECUNDAM: 25
    },
    CODEX_MP3: {
        CLAVIS_CODICIS: "libmp3lame",
        CLAVIS_RATIONIS_BITORUM: 128,
        CLAVIS_IMAGINUM_PER_SECUNDAM: 44100
    },
    CODEX_MP2: {
        CLAVIS_CODICIS: "mp2",
        CLAVIS_RATIONIS_BITORUM: 224,
        CLAVIS_IMAGINUM_PER_SECUNDAM: 44100
    },
    CODEX_VORBIS: {
        CLAVIS_CODICIS: "libvorbis",
        CLAVIS_RATIONIS_BITORUM: 64,
        CLAVIS_IMAGINUM_PER_SECUNDAM: 44100
    },
    CODEX_SPEEX: {
        CLAVIS_CODICIS: "speex",
        CLAVIS_RATIONIS_BITORUM: 32,
        CLAVIS_IMAGINUM_PER_SECUNDAM: 32000
    },
    CODEX_AAC: {
        CLAVIS_CODICIS: "aac",
        CLAVIS_RATIONIS_BITORUM: 64,
        CLAVIS_IMAGINUM_PER_SECUNDAM: 44100
    }
}

ALTITUDO_DEFALSA = 432
FORMATUS_PIXELORUM = 'yuv420p'

class NePeliculaeNeSoniFinalisError(ValueError):
    """
    Error qui emergit 
    nisi codex videalis 
    neque audialis 
    finalis 
    recognitus
    in argumentos dati sunt
    """
    def __init__(
        self,
        message=
            "Ne pelicula recognita "
            "neque sonus recognitus "
            "finalis "
            "dati sunt."
    ):
        super().__init__(message)

class NePeliculaeNeSoniOriginalisError(ValueError):
    """
    Error, qui emergit,
    nisi folium originalis
    peliculam neque sonum habet
    """
    def __init__(
        self,
        message=
            "Medium originalis "
            "ne peliculam recognitam "
            "neque sonum recognitum habet."
    ):
        super().__init__(message)

class NePeliculaeNequeSoniCommunisError(ValueError):
    """
    Error, qui emergit,
    nisi  folium originalis et finalis peliculam communem habent
    neque folium originalis et finales sonum communem habent
    """
    def __init__(
        self,
        message=
            "Medium originalis et finalis "
            "ne peliculam communem "
            "neque sonum communem "
            "habent."
    ):
        super().__init__(message)

class ConvertorisOrigo():
    "Convertoris foliorum medialium origo"
    def __init__(self, via_originalis):
        self._via_originalis = via_originalis
        # Exsistit folium datum?
        if not os.path.exists(self._via_originalis):
            # Non.
            raise FileNotFoundError(
                f'Folium datum ({self._via_originalis}) non exsistit!'
            )
        # Probamus folium
        try:
            self._proba = ffmpeg.probe(self._via_originalis)
        except ProbaeError as e:
            print('*' * 20)
            print(e.__class__.__name__)
            print('*' * 20)
            print(e)
            print('*' * 20)
            print(e.args)
            print('*' * 20)
            print(e.stdout)
            print('*' * 20)
            print(e.stderr)
            print('*' * 20)
            self._proba = None

    def da_viam_originalem(self):
        "Reddit viam ad folium originalem"
        return self._via_originalis
    via_originalis = property(da_viam_originalem)

    def da_probam(self):
        "Reddit probam folii originalis"
        return self._proba
    proba = property(da_probam)

    def da_flucem_videalem_originalem(self):
        "Reddit dictionarium flucis videalis primi"
        proba = self.proba
        if proba:
            fluces = self.proba.get('streams')
            for flux in fluces:
                if flux.get('codec_type') == 'video':
                    return flux
            return None
        return None
    flux_videalis_originalis = property(da_flucem_videalem_originalem)

    def da_flucem_audialem_originalem(self):
        "Reddit dictionarium flucis audialis primi"
        proba = self.proba
        if proba:
            fluces = proba.get('streams')
            for flux in fluces:
                if flux.get('codec_type') == 'audio':
                    return flux
            return None
        return None
    flux_audialis_originalis = property(da_flucem_audialem_originalem)

    def da_indicium_peliculae_originalis(self):
        "Indicat si folium originalis flucem videalem habet"
        return self.flux_videalis_originalis is not None
    origo_peliculam_habet = property(da_indicium_peliculae_originalis)

    def da_indicium_soni_originalis(self):
        "Indicat si folium originalis flucem audialem habet"
        return self.flux_audialis_originalis is not None
    origo_sonum_habet = property(da_indicium_soni_originalis)

    def da_dimensiones_peliculae_originalis(self):
        """
        Reddit dimensionum peliculae tuplam 
        et imaginis peliculae rationem:
        (int latitudo, int altitudo, float ratio_perfecta)
        """
        # Qaerimur flucem
        flux = self.flux_videalis_originalis
        if flux:
            # Legimus latitudinem et altitudinem
            lat = flux.get('width')
            alt = flux.get('height')
            # Legimus catenam rationis
            cat_rat = flux.get('display_aspect_ratio')
            # Habemus quamdem?
            if cat_rat:
                # Legimus latitudinem et altitudinem rationis
                lat_rat, alt_rat = cat_rat.split(':')
            else:
                lat_rat, alt_rat = lat, alt
            if lat and alt:
                return (
                    int(lat),
                    int(alt),
                    float(lat_rat) / float(alt_rat)
                )
            return None
        return None
    dimensiones_peliculae_originalis = property(
        da_dimensiones_peliculae_originalis
    )


def da_catenam_dimensionum(dimensiones: tuple):
    "Reddit catenam dimensionum datarum"
    if dimensiones:
        lat, alt = dimensiones[:2]
        return f"{lat}x{alt}"
    return None

def da_rationem_dimensionum(dimensiones: tuple):
    "Reddit rationem dimensionum datarum"
    if dimensiones:
        rat = dimensiones[2]
        return rat
    return None

class ConvertorisFinisBasis(ConvertorisOrigo):
    "Convertoris folirum nedilalium finis basis"
    def __init__( # pylint: disable=too-many-arguments
        self,
        via_originalis,
        formatus=None,
        codex_videalis=None,
        codex_audialis=None,
        magnifica=False,
        praeserva_dimensiones=False
    ):
        super().__init__(via_originalis=via_originalis)
        self._formatus = formatus
        self._codex_videalis = codex_videalis
        self._codex_audialis = codex_audialis
        self._magnifica = magnifica
        self._praeserva_dimensiones = praeserva_dimensiones

    def da_formatum(self):
        "Reddit formatum finalem"
        return self._formatus
    formatus = property(da_formatum)

    def da_indicium_formati_videalis(self):
        "Indicat si formatus pelicula sit"
        formatus = self.formatus
        return formatus in NOMINA_FORMATORUM_VIDEALIUM
    formatus_pelicula_est = property(da_indicium_formati_videalis)

    def da_indicium_formati_audialis(self):
        "Indicat si formatus sonus sit"
        formatus = self.formatus
        return formatus in NOMINA_FORMATORUM_AUDIALIUM
    formatus_sonus_est = property(da_indicium_formati_audialis)

    def da_indicium_formati_recogniti(self):
        "Indicat si formatus recognitus sit"
        formatus_pelicula_est = self.formatus_pelicula_est
        formatus_sonus_est = self.formatus_sonus_est
        return formatus_pelicula_est or formatus_sonus_est
    formatus_recognitus_est = property(da_indicium_formati_recogniti)

    def verifica_formatum_finalem(self):
        "Verificat si formatus datus regognitus sit"
        # Formatus finalis
        formatus_recognitus_est = self.formatus_recognitus_est
        if not formatus_recognitus_est:
            formatus = self.formatus
            raise ValueError(
                f"Formatus datus ({formatus}) non recognitus est."
        )

    def da_codicem_videalem(self):
        "Reddit codicem videalem"
        return self._codex_videalis
    codex_videalis = property(da_codicem_videalem)

    def da_codicem_audialem(self):
        "Reddit codicem audialem"
        return self._codex_audialis
    codex_audialis = property(da_codicem_audialem)

    def da_indicium_peliculae_finalis(self):
        "Indicat si folium finalis flucem videalem habere debet"
        return self.codex_videalis is not None
    finis_peliculam_habet = property(da_indicium_peliculae_finalis)

    def da_indicium_soni_finalis(self):
        "Indicat si folium finalis flucem audialem habere debet"
        return self.codex_audialis is not None
    finis_sonum_habet = property(da_indicium_soni_finalis)

    def da_indicium_peliculae_finalis_recognitae(self):
        "Indicat si codex finalis recognitus sit"
        return self.codex_videalis in NOMINA_CODICUM_VIDEALIUM
    codex_videalis_finalis_recognitus_est = property(
        da_indicium_peliculae_finalis_recognitae
    )

    def da_indicium_soni_finalis_recogniti(self):
        "Indicat si codex finalis recognitus sit"
        return self.codex_audialis in NOMINA_CODICUM_AUDIALIUM
    codex_audialis_finalis_recognitus_est = property(
        da_indicium_soni_finalis_recogniti
    )

    def verifica_codices_finales(self):
        "Verificat si medium finalis peliculam vel sonum habet."
        # Si finis peliculam habet
        finis_peliculam_habet = self.finis_peliculam_habet
        # Si finis sonum habet
        finis_sonum_habet = self.finis_sonum_habet
        # Si finis ne peliculam neque sonum habet:
        if not (finis_peliculam_habet or finis_sonum_habet):
            raise NePeliculaeNeSoniFinalisError()

    def verifica_codicem_videalem_finalem(self):
        "Verificat si codex videalis finalis recognitus est"
        codex_recognitus_est = self.codex_videalis_finalis_recognitus_est
        codex_videalis = self.codex_videalis
        # Si codex videalis non recognitus est:
        if not codex_recognitus_est:
            raise ValueError(
                f"Codex videalis datus ({codex_videalis}) non recognitus est."
            )
        print("Codex videalis:", codex_videalis)

    def verifica_codicem_audialem_finalem(self):
        "Verificat si codex audialis finalis recognitus est"
        codex_recognitus_est = self.codex_audialis_finalis_recognitus_est
        codex_audialis = self.codex_audialis
        # Si codex audialis non recognitus est:
        if not codex_recognitus_est:
            raise ValueError(
                f"Codex audialis datus ({codex_audialis}) non recognitus est."
            )
        print("Codex audialis:", codex_audialis)

    def da_indicium_magnificationis(self):
        "Reddit indicium, si magnificare volumus"
        return self._magnifica
    magnifica = property(da_indicium_magnificationis)

    def da_dimensionum_praeservationis_indicium(self):
        "Reddit indicium, si dimensiones praeservare volumus"
        return self._praeserva_dimensiones
    praeserva_dimensiones = property(da_dimensionum_praeservationis_indicium)

    def da_dimensiones_peliculae_finalis(self):
        "Computat dimensiones peliculae finalis"
        # Si utor magnitudinem peliculae praeservare vult,
        # conservamus dimensiones eae
        if self.praeserva_dimensiones:
            return None
        # Legimus dimensiones originales
        dimensiones_originales = self.dimensiones_peliculae_originalis
        # Habemus quædem?
        if not dimensiones_originales:
            # dimensiones invenire non potuit:
            return None
        # Discutemus dimensiones
        lat, alt, rat = dimensiones_originales
        print("Dimensiones originales:", dimensiones_originales)
        if not rat:
            rat = float(lat) / float(alt)
        max_alt = ALTITUDO_DEFALSA
        # si altitudo minor est quam maximum vel aequalis:
        if alt <= max_alt and not self.magnifica:
            lat_res = int(lat / 2) * 2  # latitudo resultans
            alt_res = int(alt / 2) * 2  # altitudo resultans
        else: # altitudo maior est quam maximum
            alt_res = ALTITUDO_DEFALSA # altitudo resultans
            lat_res = int(round(alt_res * rat))
        if lat_res % 2 == 1:
            lat_res += 1
        # dimensiones resultantes
        return (lat_res, alt_res, rat)
    dimensiones_peliculae_finalis = property(
        da_dimensiones_peliculae_finalis
    )

    def da_rationem_bitorum(self):
        "Ratio bitorum"
        codex_audialis = self.codex_audialis
        origo_sonum_habet = self.origo_sonum_habet
        finis_sonum_habet = self.finis_sonum_habet
        if origo_sonum_habet and finis_sonum_habet:
            return f"{CODICES[codex_audialis][CLAVIS_RATIONIS_BITORUM]}k"
        return None
    ratio_bitorum = property(da_rationem_bitorum)

class Convertor(ConvertorisFinisBasis):
    "Convertor foliorum medialium"
    def __init__( # pylint: disable=too-many-arguments
        self,
        via_originalis,
        formatus=None,
        codex_videalis=None,
        codex_audialis=None,
        magnifica=False,
        sic=False,
        praeserva_dimensiones=False,
        remove=False
    ):
        super().__init__(
            via_originalis=via_originalis,
            formatus=formatus,
            codex_videalis=codex_videalis,
            codex_audialis=codex_audialis,
            magnifica=magnifica,
            praeserva_dimensiones=praeserva_dimensiones
        )
        self._sic = sic
        self._remove = remove

    def da_indicium_sic(self):
        "Reddit indicium, si folium exsistentem transcribere"
        return self._sic
    sic = property(da_indicium_sic)

    def da_indicium_remotionis(self):
        "Reddit indicium, si utor folium originalem removere vult"
        return self._remove
    remove = property(da_indicium_remotionis)

    def da_viam_finalem(self):
        "Reddit viam ad folium finalem"
        # Pæparamus viam resultantem
        basis_viae = os.path.splitext(self._via_originalis)[0]
        # Delemus catenam ".xvid"
        if basis_viae.endswith('.xvid'): # extensio ".xvid.avi"
            basis_viae = os.path.splitext(basis_viae)[0]
        if basis_viae.endswith('.mpeg4'): # extensio ".mpeg4.avi"
            basis_viae = os.path.splitext(basis_viae)[0]
        # Basis viæ unicodita
        if isinstance(basis_viae, bytes):
            basis_viae_unicodita = basis_viae.decode(FSENC)
        else:
            basis_viae_unicodita = basis_viae
        # Addimus extensionem
        via_finalis = ".".join(
            [
                basis_viae_unicodita,
                self.formatus
            ]
        )
        return via_finalis
    via_finalis = property(da_viam_finalem)

    def da_medium_finalem(self, basis_medii_finalis):
        "Reddit obiectum medii finalis ex base eius"
        sic = self.sic
        if sic:
            return basis_medii_finalis.overwrite_output()
        return basis_medii_finalis

    def converte(self):
        """
        Convertit peliculam in folio ad viam datam
        utens formatum videalem et audialem datum.
        """
        # Si medium originalis peliculam habet
        origo_peliculam_habet = self.origo_peliculam_habet
        # Si medium originalis sonum habet
        origo_sonum_habet = self.origo_sonum_habet
        # Si medium finalis peliculam habere debet
        finis_peliculam_habet = self.finis_peliculam_habet
        # Si medium finalis sonum habere debet
        finis_sonum_habet =self.finis_sonum_habet
        # Codex videalis
        codex_videalis = self.codex_videalis
        # Codex audialis
        codex_audialis = self.codex_audialis
        # Verificamus formatum finalem
        self.verifica_formatum_finalem()
        # Verificamus codices finales
        self.verifica_codices_finales()
        # Verificamus si codex videalis finalis recognitus sit
        self.verifica_codicem_videalem_finalem()
        # Verificamus si codex audialis finalis recognitus sit
        self.verifica_codicem_audialem_finalem()
        # Computamus magnitudinem optimam peliculae resultantis
        dimensiones_finales = self.dimensiones_peliculae_finalis
        # Catena dimensionum
        catena_dimensionum = da_catenam_dimensionum(dimensiones_finales)
        # Ratio dimensionum
        ratio_dimensionum = da_rationem_dimensionum(dimensiones_finales)
        # Ratio bitorum
        ratio_bitorum = self.ratio_bitorum
        # Via originalis
        via_originalis = self.via_originalis
        # Via finalis
        via_finalis = self.via_finalis
        # Pelicula originalis
        medium_originalis = ffmpeg.input(via_originalis)
        # Pelicula finalis
        if origo_peliculam_habet and finis_peliculam_habet \
        and origo_sonum_habet and finis_sonum_habet:
            basis_medii_finalis = ffmpeg.output(
                medium_originalis,
                via_finalis,
                vcodec=codex_videalis,
                pix_fmt=FORMATUS_PIXELORUM,
                r=CODICES[codex_videalis][
                    CLAVIS_IMAGINUM_PER_SECUNDAM
                ],
                s=catena_dimensionum,
                aspect=ratio_dimensionum,
                acodec=codex_audialis,
                audio_bitrate=ratio_bitorum,
                ar=CODICES[codex_audialis][CLAVIS_IMAGINUM_PER_SECUNDAM],
                ac=2,
                scodec='copy'
            )
        elif origo_peliculam_habet and finis_peliculam_habet: # pelicula sola
            basis_medii_finalis = ffmpeg.output(
                medium_originalis,
                via_finalis,
                vcodec=codex_videalis,
                pix_fmt=FORMATUS_PIXELORUM,
                r=CODICES[codex_videalis][
                    CLAVIS_IMAGINUM_PER_SECUNDAM
                ],
                s=catena_dimensionum,
                aspect=ratio_dimensionum,
                an=None,
                scodec='copy'
            )
        elif origo_sonum_habet and finis_sonum_habet: # sonus solus
            basis_medii_finalis = ffmpeg.output(
                medium_originalis,
                via_finalis,
                vn=None,
                acodec=codex_audialis,
                audio_bitrate=ratio_bitorum,
                ar=CODICES[codex_audialis][
                    CLAVIS_IMAGINUM_PER_SECUNDAM
                ],
                ac=2,
                scodec='copy'
            )
        else: # ne pelicula neque sonus
            raise NePeliculaeNequeSoniCommunisError()
        # Medium finalis
        medium_finalis = self.da_medium_finalem(basis_medii_finalis)
        medium_finalis.run()
        print(medium_finalis)
        remove = self._remove
        if remove:
            os.remove(via_originalis)
        return 0

class AnalysatorisArgumentorumBasis(argparse.ArgumentParser):
    "Analysat argumentos ad programma FF datos"
    def __init__(self, programma=None, descriptio=None):
        super().__init__(
            prog=programma,
            description=descriptio
        )
        self.add_argument(
            '-f', '--formatus',
            default=None,
            help="formatus"
        )
        self.add_argument(
            '-v', '--codex-videalis',
            default=None,
            help="codex videalis"
        )
        self.add_argument(
            '-a', '--codex-audialis',
            default=None,
            help="codex audialis"
        )
        self.add_argument(
            '-m', '--magnifica',
            action="store_true",
            default=False,
            help="si peliculam parvam magnificare"
        )
        self.add_argument(
            '-s', '--sic',
            action="store_true",
            default=False,
            help="si peliculam exsistentem transcribere"
        )
        self.add_argument(
            '-p', '--praeserva-dimensiones',
            action="store_true",
            default=False,
            help="si dimensiones præservare"
        )
        self.add_argument(
            '-r', '--remove',
            action="store_true",
            default=False,
            help="si folium indendum removere"
        )

class AnalysatorArgumentorumProgrammatisFF(AnalysatorisArgumentorumBasis):
    "Analysat argumentos ad programma FF datos"
    def __init__(self, programma=None, descriptio=None):
        super().__init__(programma, descriptio)
        self.add_argument(
            'folium',
            help="folium peliculae"
        )

if __name__ == '__main__':
    if os.name == 'nt':
        for i, j in enumerate(sys.argv):
            if isinstance(j, bytes):
                sys.argv[i] = j.decode(FSENC)

    PARSER = AnalysatorArgumentorumProgrammatisFF(
        programma="ff",
        descriptio="Convertit peliculas programma FFmpeg utens simpliciter"
    )
    ARGS = PARSER.parse_args()

    VIA = ARGS.folium
    FORMATUS = ARGS.formatus
    CODEX_VIDEALIS = ARGS.codex_videalis
    CODEX_AUDIALIS = ARGS.codex_audialis
    MAGNIFICA = ARGS.magnifica
    SIC = ARGS.sic
    PRAESERVA = ARGS.praeserva_dimensiones
    REMOVE = ARGS.remove

    CONVERTOR = Convertor(
        via_originalis=VIA,
        formatus=FORMATUS,
        codex_videalis=CODEX_VIDEALIS,
        codex_audialis=CODEX_AUDIALIS,
        magnifica=MAGNIFICA,
        sic=SIC,
        praeserva_dimensiones=PRAESERVA,
        remove=REMOVE
    )

    CODEX = CONVERTOR.converte()

    sys.exit(CODEX)
