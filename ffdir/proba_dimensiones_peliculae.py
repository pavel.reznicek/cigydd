#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import re
import argparse
import subprocess

EXPRESSIO = re.compile(u"""
  Stream              # flux
  \\s+                # spatium
  [#]                 # crucillus, signum indicis
  #\\d+[.]\\d+         # index flucis
  \\d+[:]\\d+         # index flucis
  .*                  # quodlibet, e. g. „(und)“
  :                   # colon
  \\s+                # spatium
  Video:              # pelicula
  \\s+                # spatium
  \\w+                # codecus
  .*                  # quodlibet, e. g. „ (Main) (WMV3 / 0x33564D57)“
  ,                   # comma
  \\s+                # spatium
  \\w+                # formatus pixelorum
  (?:[(].*[)])?       # quodlibet inter parentheses,
                      # e. g. „(tv, bt709/unknown/unknown)“
  ,                   # comma
  \\s+                # spatium
  (?P<latitudo>\\d+)  # latitudo
  x                   # x-crucillus
  (?P<altitudo>\\d+)  # altitudo
  (
    .*                  # quodlibet
    #,                  # comma
    #\\s+                # spatium
    #.*                  # quodlibet
    \\s+                # spatium
    DAR                 # Display Aspect Ratio - ratio aspecti imaginis
    \\s+                # spatium
    (?P<ratio_latitudinis>\\d+)
    :
    (?P<ratio_altitudinis>\\d+)
  )?
""",
                       re.VERBOSE)

FSENC = sys.getfilesystemencoding()

def da_dimensiones_peliculae(via):
    """Dat dimensionum peliculae tuplam et imaginis peliculae rationem:
    (int latitudo, int altitudo, float ratio_perfecta)"""
    cfds = os.name != 'nt'
    if isinstance(via, str):
        via_encodita = via
    elif isinstance(via, unicode):
        via_encodita = via.encode(FSENC)
    else:
        via_encodita = via
    if os.path.exists(via):
        try:
            pipa = subprocess.Popen(['ffprobe', via_encodita],
                                    stdin=subprocess.PIPE,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE,
                                    close_fds=cfds)
        except IOError:
            try:
                print type(via)
                pipa = subprocess.Popen(['ffmpeg', '-i', via_encodita],
                                        stdin=subprocess.PIPE,
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE,
                                        close_fds=cfds)
            except IOError as error1:
                nuntius = """Error in reseratione pipae.
Classa erroris: %s,
nuntius: „%s“,
fsenc: %s"""%(error1.__class__.__name__, error1.message, FSENC)
                print nuntius
                return None
        pipa.wait()
        if pipa.stderr:
            emissio = pipa.stderr.read().decode(FSENC)
        else: # pipa nullum flucem erroricum standardum habet:
            emissio = u'' # sicut programma nil dixeret.
        print u"""
emissio:
%s
"""%emissio
        inventio = EXPRESSIO.search(emissio)
        if inventio:
            #print inventio.group(u"latitudo"), inventio.span(u"latitudo")
            #print inventio.group(u"altitudo"), inventio.span(u"altitudo")
            lat = inventio.group(u"latitudo")
            alt = inventio.group(u"altitudo")
            rat_lat = inventio.group(u"ratio_latitudinis")
            if not rat_lat:
                rat_lat = lat
            rat_alt = inventio.group(u"ratio_altitudinis")
            if not rat_alt:
                rat_alt = alt
            return (int(lat), int(alt), float(rat_lat) / float(rat_alt))
        else:
            print u"Flux videalis non inventus est."
            return None
    else: # folium non exsistit
        return None


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("via")
    args = parser.parse_args()

    via = args.via
    print "via:", via

    catena_dimensionum = da_dimensiones_peliculae(via)

    print "catena_dimensionum:", catena_dimensionum
