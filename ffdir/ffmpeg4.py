#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, re, sys, subprocess
from moje.listtext import printdir

def da_dimensiones_peliculae(via):
  """Dat tuplam dimensionum peliculae et rationem imaginis peliculae:
  (int latitudo, int altitudo, float ratio_perfecta)"""
  if os.path.exists(via):
    pipa = subprocess.Popen([u'ffprobe', via],
      stdin=subprocess.PIPE,
      stdout=subprocess.PIPE,
      stderr=subprocess.PIPE,
      close_fds=True)
    cos = pipa.wait()
    if cos != 0:
      pipa = subprocess.Popen([u'ffmpeg', u'-i', via])
      cos = pipa.wait()
    if pipa.stderr:
      emissio = pipa.stderr.read()
    else: # pipa nullum flucem erroricum standardum habet:
      emissio = u'' # sicut programma nil dixeret.
#print u"""
#emissio:
#%s
#"""%emissio
    expressio = re.compile(u"""
      Stream              # flux
      \\s+                # spatium
      [#]                 # crucillus, signum indicis
      \\d+[.]\\d+:        # index flucis
      \\s+                # spatium
      Video:              # pelicula
      \\s+                # spatium
      \\w+                # codecus
      ,                   # comma
      \\s+                # spatium
      \\w+                # formatus pixelorum
      ,                   # comma
      \\s+                # spatium
      (?P<latitudo>\\d+)  # latitudo
      x                   # x-crucillus
      (?P<altitudo>\\d+)  # altitudo
      .*                  # quodlibet
      #,                  # comma
      #\\s+                # spatium
      #.*                  # quodlibet
      \\s+                # spatium
      DAR                 # Display Aspect Ratio - ratio aspecti imaginis
      \\s+                # spatium
      (?P<ratio_latitudinis>\\d+)
      :
      (?P<ratio_altitudinis>\\d+)
      """,
      re.VERBOSE)
    inventio = expressio.search(emissio)
    if inventio:
      #print inventio.group(u"latitudo"), inventio.span(u"latitudo")
      #print inventio.group(u"altitudo"), inventio.span(u"altitudo")
      l = inventio.group(u"latitudo")
      a = inventio.group(u"altitudo")
      rl = inventio.group(u"ratio_latitudinis")
      ra = inventio.group(u"ratio_altitudinis")
      return (int(l), int(a), float(rl) / float(ra))
    else:
      print u"Flux videalis non inventus est."
      return None
  else: # folium non exsistit
    return None
    
def converte(via):
  if os.path.exists(via):
    # Computamus magnitudinem optimam peliculae resultandae:
    dimensiones = da_dimensiones_peliculae(via)
    if dimensiones:
      l, a, r = dimensiones
      if l <= 720: # latitudo minor est quam DCCXX:
        catena_dimensionum = u'' # dimensiones conservamus
      else:
        lr = 720 # latitudo resultans
        ar = int(round(lr / r)) # altitudo resultans
        catena_dimensionum = \
          u'-s %(lr)sx%(ar)s -aspect %(r)s'%{u'lr': lr, u'ar': ar, u'r': r}
    else: # dimensiones invenire non potuit:
      catena_dimensionum = u'' # dimensiones conservamus
    basis_viae = os.path.splitext(via)[0]
    if basis_viae.endswith(u'.xvid'):
      basis_viae = os.path.splitext(basis_viae)[0]
    via_resultans = basis_viae + u".mpeg4.avi"
    fsenc = sys.getfilesystemencoding()
    if os.name == 'nt':
      os.system(
        (u'ffmpeg -i "%s" -vcodec mpeg4 -b 1024k %s -acodec mp2 -ab 128k -ac 2 "%s"'
        %(via, catena_dimensionum, via_resultans)).encode(fsenc)
        )
    else:
      os.system(
        (u'ffmpeg -i "%s" -vcodec mpeg4 -b 1024k %s -acodec mp2 -ab 128k -ac 2 "%s"'
        %(via, catena_dimensionum, via_resultans)).encode(fsenc)
        )
  else: # folium non exsistit:
    print u"Folium datum (%s) non exsistit."%via

if __name__ == '__main__':
  #print sys.argv
  if len(sys.argv) == 2:
    # Ad experimentum:
    #print da_dimensiones_peliculae(sys.argv[1])
    converte(sys.argv[1])
  else:
    print u"""
ffmpeg4.py - scriptus convertens peliculam fonticam
             in peliculam in formato AVI
             incodito codecis MPEGIV et MPII
             cum latitudine maximali DCCXX pixelorum
Usus:
ffmpeg4.py <folium_fonticum>
"""
