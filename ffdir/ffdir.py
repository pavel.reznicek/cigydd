#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
ffdir.py – scriptum convertens media fontica
           in directorio dato
           in peliculas incoditas codecis datis
           in formato coniuncto
           cum altitudine imaginis maximali firme obiciata
"""

import os
import sys
from ffmpeg._probe import Error as FFProbeError
from ffmpeg._run import Error as FFRunError
import ff


# arg: sequentio codecorum: (formatus_videalis, formatus_audialis)
def converte_directorium(
        nomen_directorii,
        nomina_foliorum,
        argumenta
    ):
    """Convertit peliculas recte residentes in directorio uno"""
    formatus = argumenta.formatus
    nomina_foliorum.sort()
    for nomen_folii in nomina_foliorum:
        via = os.path.join(nomen_directorii, nomen_folii)
        if not (os.path.isdir(via) or via.endswith('.' + formatus)):
            print()
            print(
                '-------',
                via,
                '-------'
            )
            print()
            try:
                convertor = ff.Convertor(
                    via_originalis=via,
                    formatus=formatus,
                    codex_videalis=argumenta.codex_videalis,
                    codex_audialis=argumenta.codex_audialis,
                    magnifica=argumenta.magnifica,
                    sic=argumenta.sic,
                    praeserva_dimensiones=argumenta.praeserva_dimensiones,
                    remove=argumenta.remove
                )
            except FFProbeError:
                continue
            codex_redditus = 1
            try:
                codex_redditus = convertor.converte()
            except FFRunError:
                continue
            except ff.NePeliculaeNeSoniOriginalisError as e:
                print(e)
                continue
            except ff.NePeliculaeNeSoniFinalisError as e:
                print(e)
                continue
            if codex_redditus != 0:
                sys.exit(codex_redditus)


class AnalysatorArgumentorumProgrammatisFFDir(
    ff.AnalysatorisArgumentorumBasis
):
    "Analysat argumenta data ad programma FFDir"
    def __init__(self, programma="ffdir", descriptio=__doc__):
        super().__init__(programma, descriptio)
        self.add_argument(
            '-d', '--directorium',
            default=os.getcwd(),
            help='directorium convertendum'
        )


if __name__ == '__main__':
    PARSER = AnalysatorArgumentorumProgrammatisFFDir()
    ARGS = PARSER.parse_args()
    DIRECTORIUM = ARGS.directorium
    FORMATUS = ARGS.formatus
    CODEX_VIDEALIS = ARGS.codex_videalis
    CODEX_AUDIALIS = ARGS.codex_audialis
    MAGNIFICA = ARGS.magnifica
    SIC = ARGS.sic
    PRAESERVA_DIMENSIONES = ARGS.praeserva_dimensiones
    REMOVE = ARGS.remove
    print(
        """ffdir:
Formatus videalis:     %(formatus videalis)s
Formatus audialis:     %(formatus audialis)s
Directorium:           %(directorium)s
Magnifica:             %(magnifica)s
Sic:                   %(sic)s
Praeserva dimensiones: %(praeserva dimensiones)s
"""%{
        "formatus videalis": CODEX_VIDEALIS,
        "formatus audialis": CODEX_AUDIALIS,
        "directorium": DIRECTORIUM,
        "magnifica": MAGNIFICA,
        "sic": SIC,
        "praeserva dimensiones": PRAESERVA_DIMENSIONES
    }
    )
    for subdirectorium, lista_subsubdirectoriorum, lista_foliorum \
    in os.walk(DIRECTORIUM):
        converte_directorium(
            subdirectorium,
            lista_foliorum,
            ARGS
        )
