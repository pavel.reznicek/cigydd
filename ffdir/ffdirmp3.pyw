# -*- coding: utf-8 -*-

import os
import sys
import locale

def fn(arg, dirname, fnames):
  fnames.sort()
  for s in fnames:
    cesta = (dirname + os.path.sep + s)
    if not cesta.endswith(u".mp3"):
      zaklad = os.path.splitext(cesta)[0]
      mp3 = zaklad + u".mp3"
      print()
      print(
	u'------- %s -------'%(
          mp3#.decode(locale.getpreferredencoding(), 'replace')
          )
        )

      print()
      os.system(
        (u'ffmpeg -i "%s" -ab 192k -ar 44100 "%s"'%(cesta, mp3))\
        #.encode(locale.getpreferredencoding(), 'replace')
        )
if __name__ == '__main__':
  for root, dirs, files in os.walk(u"."):
    fn(None, root, files)

