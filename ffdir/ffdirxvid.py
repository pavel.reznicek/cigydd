#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from os import path
import sys
import locale

def fn(arg, dirname, fnames):
  fnames.sort()
  for s in fnames:
    cesta = (dirname + path.sep + s)
    if not (path.isdir(cesta) or cesta.endswith(u".xvid.avi")):
      zaklad = path.splitext(cesta)[0]
      xvid = zaklad + u".xvid.avi"
      print 
      print '-------', xvid.encode(locale.getpreferredencoding(), 'replace'), '-------'
      print
      if os.name == 'nt':
        os.system(
          (u'ffmpeg -i "%s" -vcodec libxvid -b 1024k -acodec libmp3lame -ab 128k "%s"'
          % (cesta, xvid)).encode(sys.getfilesystemencoding())
          )
      else:
        os.system(
          (u'ffmpeg -i "%s" -vcodec libxvid -b 1024k -acodec libmp3lame -ab 128k -ac 2 "%s"'
          % (cesta, xvid)).encode(sys.getfilesystemencoding())
          )
path.walk(u".", fn, None)

