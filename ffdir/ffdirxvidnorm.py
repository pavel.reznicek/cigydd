#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from os import path
import sys
import locale

def fn(arg, dirname, fnames):
  fnames.sort()
  for s in fnames:
    cesta = (dirname + path.sep + s)
    if not (path.isdir(cesta) or cesta.endswith(u".xvid.avi")):
      zaklad = path.splitext(cesta)[0]
      xvid = zaklad + u".xvid.avi"
      wav = zaklad + u".wav"
      print 
      print '-------', xvid, '-------'
      print
      os.system(
        (u'ffmpeg -y -i "%s" "%s"'
        % (cesta, wav)).encode(locale.getpreferredencoding())
	)
      os.system(
        (u'normalize -a 0,99 "%s"'
	% (wav)).encode(locale.getpreferredencoding())
	)
      os.system(
        (u'ffmpeg -y -i "%s" -i "%s" -vcodec libxvid -b 1024k -acodec libmp3lame -ab 128k -map 0:0 -map 1:0 "%s"'
        % (cesta, wav, xvid)).encode(locale.getpreferredencoding())
        )
      if os.path.exists(wav):
        os.remove(wav)
path.walk(u".", fn, None)

