#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""ffdirmp4
A script to convert various video files to the mp4 format.
"""


import os
from cigydd.ffdir import ff

EXTENSIO = ff.FORMATUS_MP4

if __name__ == '__main__':

    for root, dirnames, fnames in os.walk(os.curdir):
        for fname in sorted(fnames):
            if not fname.endswith("." + EXTENSIO):
                path = os.path.join(root, fname)
                if not os.path.isdir(path):
                    print()
                    print('-------', path, '-------')
                    print()
                convertor = ff.Convertor(
                    via_originalis=path,
                    formatus=EXTENSIO,
                    codex_videalis=ff.CODEX_X264,
                    codex_audialis=ff.CODEX_AAC
                )
                convertor.converte()
