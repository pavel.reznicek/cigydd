#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import locale

def func(arg, dirname, fnames):
  fnames.sort()
  for s in fnames:
    path = (dirname + os.path.sep + s)
    if not path.endswith(u".ogg"):
      base = os.path.splitext(path)[0]
      ogg = base + u".ogg"
      print()
      print('-------', ogg, '-------')
      print()
      os.system(
        (u'ffmpeg -i "%s" -ab 192k -ar 44100 -acodec libvorbis "%s"'%(path, ogg))\
        .encode(locale.getpreferredencoding())
        )
os.path.walk(u".", func, None)

