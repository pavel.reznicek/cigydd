#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import locale

def fn(arg, dirname, fnames):
  fnames.sort()
  for s in fnames:
    cesta = (dirname + os.path.sep + s)
    if not cesta.endswith(".mp3"):
      zaklad = os.path.splitext(cesta)[0]
      mp3 = zaklad + ".mp3"
      print()
      print(
	'------- %s -------'%(
        	mp3#.decode(locale.getpreferredencoding(), 'replace')
        	)
	)
        
      print()
      os.system(
        (u'ffmpeg -i "%s" -ab 192k -ar 44100 "%s"'%(cesta, mp3))\
        .encode(locale.getpreferredencoding(), 'replace')
        )
os.walk(u".", fn, None)

