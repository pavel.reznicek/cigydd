#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from os import path
import sys
import locale

def fn(arg, dirname, fnames):
  fnames.sort()
  for s in fnames:
    cesta = (dirname + path.sep + s)
    if not (path.isdir(cesta) or  cesta.endswith(u".mpeg2.avi")):
      zaklad = path.splitext(cesta)[0]
      mpeg2 = zaklad + u".mpeg2.avi"
      wav = zaklad + u".wav"
      print 
      print '-------', mpeg2, '-------'
      print
      os.system(
        (u'ffmpeg -y -i "%s" "%s"'
        % (cesta, wav)).encode(locale.getpreferredencoding())
	)
      os.system(
        (u'normalize -a 0,99 "%s"'
	% (wav)).encode(locale.getpreferredencoding())
	)
      os.system(
        (u'ffmpeg -y -i "%s" -i "%s" -acodec libmp3lame -ar 44100 -ac 2 -ab 128k -vcodec mpeg2video -b 1024k -map 0:0 -map 1:0 "%s"'
        % (cesta, wav, mpeg2)).encode(locale.getpreferredencoding())
        )
      if os.path.exists(wav):
        os.remove(wav)
path.walk(u".", fn, None)

