#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from os import path
import sys
import locale

def fn(arg, dirname, fnames):
  fnames.sort()
  for s in fnames:
    cesta = (dirname + path.sep + s)
    if not cesta.endswith(u".wav"):
      zaklad = path.splitext(cesta)[0]
      wav = zaklad + u".wav"
      print 
      print '-------', wav, '-------'
      print
      os.system(
        (u'ffmpeg -i "%s" -ab 1024k -ar 44100 "%s"'%(cesta, wav))\
        .encode(locale.getpreferredencoding())
        )
path.walk(u".", fn, None)

