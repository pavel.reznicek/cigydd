#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import locale
import ff

EXTENSIO = ff.FORMATI[ff.FORMATUS_MPEG4][ff.CLAVIS_EXTENSIONIS]

def fn(arg, dirname, fnames):
  fnames.sort()
  for s in fnames:
    cesta = (dirname + os.path.sep + s)
    if not (os.path.isdir(cesta) or cesta.endswith(EXTENSIO)):
      print 
      print '-------', \
        cesta.encode(locale.getpreferredencoding(), 'replace'), \
        '-------'
      print
      ff.converte(cesta, ff.FORMATUS_MPEG4, ff.FORMATUS_MP2)
os.path.walk(u".", fn, None)

