#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from os import path
import sys
import locale

def fn(arg, dirname, fnames):
  fnames.sort()
  for s in fnames:
    cesta = (dirname + path.sep + s)
    if not cesta.endswith(u".flac"):
      zaklad = path.splitext(cesta)[0]
      flac = zaklad + u".flac"
      print 
      print '-------', flac, '-------'
      print
      os.system(
        (u'ffmpeg -i "%s" -ab 192k -ar 44100 "%s"'%(cesta, flac))\
        .encode(locale.getpreferredencoding())
        )
path.walk(u".", fn, None)

