#!/usr/bin/env python
# -*- coding: utf8 -*-

import sys

class adder:
  v=0
  def __init__(self):
    self.v=0
    def getvalue(self):
      return self.v
    def setvalue(self,val):
      self.v=val
    def delvalue(self,val):
      del(self.get_value,self.set_value,self.del_value,self.v,self.value)
    def add(self,val):
      self.v+=val
    def subtract(self,val):
      self.v-=val
    value=property(getvalue,setvalue,delvalue,u"pokusná třída na property")
    
def Property(function):
  u"""Dekorátor Property
    Použití:
    class trida:
      @Property
      def funkce():
        def fget(self):
          ...
        def fset(self,val):
          ...
        def fdel(self):
          ...
        def fdoc(self):
          ...
"""
  keys = 'fget', 'fset', 'fdel'
  func_locals = {'doc':function.__doc__}
  def probeFunc(frame, event, arg):
    if event == 'return':
      locals = frame.f_locals
      func_locals.update(dict((k,locals.get(k)) for k in keys))
      sys.settrace(None)
    return probeFunc
  sys.settrace(probeFunc)
  function()
  return property(**func_locals)



class tester(object):
  def __init__(ego):
    ego.v = None
  @Property
  def vlastnost():
    def fget(ego):
      return ego.v
    def fset(ego, hodnota):
      ego.v = hodnota