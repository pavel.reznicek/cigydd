import os

def dirtext(obj):
  s=""
  for it in dir(obj):
    s += str(it) + os.linesep
  return s
  
def printdir(obj):
  for it in dir(obj):
    print(it)

def printlist(lst):
  for it in lst:
    print(it)

def listtext(lst):
  result = ""
  for it in lst:
    result += str(it) + os.linesep
  return result

