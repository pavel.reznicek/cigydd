#!/usr/bin/env python

import wx
import wx.media
import os

class SimpleMediaPlayerControl(wx.media.MediaCtrl):
    
    def __init__(self, parent, file_path=wx.EmptyString):
        self._file_path = file_path

        if 'wxMSW' in wx.PlatformInfo:
            backend = wx.media.MEDIABACKEND_WMP10
        elif 'wxMac' in wx.PlatformInfo:
            backend = wx.media.MEDIABACKEND_QUICKTIME
        elif 'wxGTK' in wx.PlatformInfo:
            backend = wx.media.MEDIABACKEND_GSTREAMER
        self._backend = backend

        wx.media.MediaCtrl.__init__(
            self, parent, -1, 
            fileName=file_path, szBackend=backend
        )
        
    
    @property
    def FilePath(self):
        return self._file_path
    @FilePath.setter
    def FilePath(self, file_path):
        self._file_path = file_path
        if file_path:
            self.Load(file_path)
        else:
            self.Close()
    
    @property
    def Backend(self):
        return self._backend

class OneTimeMediaPlayer(wx.Frame):
    def __init__(self, file_path=wx.EmptyString):
        super().__init__(
            parent=None, 
            style=wx.DEFAULT_FRAME_STYLE | wx.ICONIZE
        )
        self._smpc = SimpleMediaPlayerControl(self, file_path)

        self._smpc.Bind(wx.media.EVT_MEDIA_LOADED, self.OnMediaLoaded)
        self._smpc.Bind(wx.media.EVT_MEDIA_FINISHED, self.OnMediaFinished)

        loaded = self._smpc.Load(file_path)
        print('Loaded:', loaded)

    def OnMediaLoaded(self, evt):
        self._smpc.Play()
    
    def OnMediaFinished(self, evt):
        self.Close()
    
    def Play(self):
        self._smpc.Play()

class TestApp(wx.App):
    def OnInit(self):
        moddir = os.path.dirname(__file__)
        file_path = os.path.join(moddir, 'pyp', 'pyp.wav')
        player = OneTimeMediaPlayer(file_path)
        return True

if __name__ == '__main__':
    APP = TestApp()
    APP.MainLoop()
