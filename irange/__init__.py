#!/usr/bin/env python3
#-*- coding: utf-8 -*-
"""irange package
==============
Defines an intelligent range (irange) function that works like the normal range function
but contains the ending number, i. e.

range(5)  -> (0, 1, 2, 3, 4)
irange(5) -> (0, 1, 2, 3, 4, 5)
"""

def irange(stop_or_start=0, stop=None, step=None):
    if stop_or_start == None:
        stop_or_start = 0
    if stop == None:
        if step == None:
            result = range(0, stop_or_start + 1)
        else:
            result = range(0, stop_nebo_start + 1, step)
    else:
        if step == None:
            result = range(stop_or_start, stop + 1)
        else:
            result = range(stop_or_start, stop + 1, step)
    return result