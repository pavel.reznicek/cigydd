#!/usr/bin/env python3
# -*- coding:utf8 -*-

"""A library for easing work with graphics in wxPython.
The basic classes are InitialisedMemoryDC
and SelfRepaintingClientDC."""

import wx
import cigydd
from cigydd.graphics import fasis


class InitialisedMemoryDC(wx.MemoryDC):
    "An offscreen device context based on a bitmap."
    def __init__(self, width, height):
        wx.MemoryDC.__init__(self)
        if wx.__version__ in ('3.0.3', '4.0.1'):
            bitmap = self.bitmap = wx.Bitmap(width, height)
        else:
            print("debug: wx.__version__:", wx.__version__)
            bitmap = self.bitmap = wx.EmptyBitmap(width, height)
        self.SelectObject(bitmap)
        self.Clear()

    def get_bitmap(self):
        "The Bitmap property getter."
        return self.bitmap
    def set_bitmap(self, bitmap):
        "The Bitmap property setter."
        self.bitmap = bitmap
        self.SelectObject(bitmap)
    Bitmap = property(get_bitmap, set_bitmap)


class GraphicsTestForm(wx.Frame): # pylint: disable=too-many-ancestors
    "A form to test the graphics. Used in the GraphicsTestApp."
    def __init__(self, parent=None):
        wx.Frame.__init__(self, parent, title="cigydd.graphics.graphics module test")
        # Set instance variables
        self._imdc = None
        self._running = False

        # This is crucial for a regular painting with a paint event:
        self.SetBackgroundStyle(wx.BG_STYLE_PAINT)

        # Bind events to their handlers
        self.Bind(wx.EVT_LEFT_DOWN, self.on_click)
        self.Bind(wx.EVT_PAINT, self.on_paint)
        self.Bind(wx.EVT_SIZE, self.on_size)

    def on_size(self, evt):
        "The resize event handler"
        new_width, new_height = self.ClientSize # pylint: disable=unpacking-non-sequence
        if not self._imdc:
            self._imdc = InitialisedMemoryDC(new_width, new_height)
        else:
            old_width, old_height = self._imdc.Size
            if new_width > old_width or new_height > old_height:
                new_imdc = InitialisedMemoryDC(new_width, new_height)
                new_imdc.Blit(0, 0, old_width, old_height, self._imdc, 0, 0)
                self._imdc = new_imdc
        self._imdc.Background.Colour = self.BackgroundColour
        evt.Skip()

    def on_paint(self, evt):
        "The paint event handler"
        window = evt.GetEventObject()
        pdc = wx.AutoBufferedPaintDC(window)
        upd = wx.RegionIterator(window.GetUpdateRegion())
        imdc = self._imdc
        while upd:
            rect = upd.GetRect()
            xcoord, ycoord, width, height = rect
            pdc.Blit(xcoord, ycoord, width, height, imdc, xcoord, ycoord)
            upd.Next()
        evt.Skip()

    def on_click(self, evt):
        "The form's click event handler"
        if self._running:
            return
        self._running = True
        imdc = self._imdc
        width, height = self.ClientSize # pylint: disable=unpacking-non-sequence
        for ycoord in range(height):
            pens = []
            points = []
            for xcoord in range(width):
                points.append((xcoord, ycoord))
                [xarg, yarg] = [n / 2.0 for n in (xcoord - width / 2.0, ycoord - height / 2.0)]
                # pen = wx.Pen(wx.Colour(xarg % 256, yarg % 256, xarg * yarg % 256))
                try:
                    colour = fasis.fasis(xarg / yarg + yarg / 100,
                                         [0x0000FF, 0xFF0000, 0x00FF00],
                                         1, fasis.FAS_MIRROR)
                    pen = wx.Pen(wx.Colour(colour))
                except ZeroDivisionError:
                    pen = wx.BLACK_PEN
                pens.append(pen)
            self.SetBackgroundColour(wx.BLACK)
            imdc.DrawPointList(points, pens)
            rect = (0, ycoord, width, 1)
            self.RefreshRect(rect)
            wx.Yield()
        cigydd.Pyp()
        self._running = False
        evt.Skip()

class GraphicsTestApp(wx.App):
    "An application to test the graphics module"
    def __init__(self):
        wx.App.__init__(self, redirect=False)
        _fra = GraphicsTestForm(None)
        self.SetTopWindow(_fra)
        _fra.Show()

    def get_frame(self):
        "The Frame property getter"
        return self._fra
    Frame = property(get_frame)


if __name__ == "__main__":
    APP = GraphicsTestApp()
    APP.MainLoop()
