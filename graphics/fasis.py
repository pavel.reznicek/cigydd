#!/usr/bin/env python3
# -*- coding:utf8 -*-
"""fasis - a module defining functions for computing color scales
and transitions"""

import wx
from cigydd.lists import rotate_list

FAS_NOTHING = 0
FAS_MIRROR = 1
FAS_RETURNING_SCALE = 2

def fasis(fxy, scale=(0x000000, 0xffffff), step=1, flags=0):
    """fasis(fxy, scale=(0x000000,0xffffff), step=1, flags=0) -> RGB int
    Returns a transition between two neighboring colors from the _scale_ tuple
    rationally after input value _fxy_. _scale_ repeats forever
    and the part between the two _scale_ colors in it corresponds to _step_ units"""

    #the sign of the input value of the function f(x,y)
    sign = sgn(fxy)

    if FAS_MIRROR & flags == FAS_MIRROR:
        fxy = abs(fxy)
        reverse_order = False
    else:   # don't mirror:
        if sign == -1:
            reverse_order = True
            scale = reversed(scale)
            scale = rotate_list(scale, 1)
        else:
            reverse_order = False


    if FAS_RETURNING_SCALE & flags == FAS_RETURNING_SCALE:
        returnback = list(scale[:-1])
        returnback = reversed(returnback)
        modscale = scale + returnback
    else:
        modscale=scale + scale[:1]

    colorcount=len(modscale)-1


    period = float(step) * colorcount

    period_modulus = fxy % float(period)

    step_modulus = fxy % float(step)

    in_steps = period_modulus / float(step)

    color_ratio = step_modulus / float(step)

    c1_index = int(in_steps)
    c2_index = c1_index + 1

    if reverse_order:
        c1_index = colorcount - c1_index
        c2_index = colorcount - c2_index

    return intercolour(modscale[c1_index], modscale[c2_index], color_ratio)


def intercolour(colour1, colour2, ratio):
    c1wxc = wx.Colour(colour1)
    c2wxc = wx.Colour(colour2)
    c1rgb = c1wxc.Get()
    c2rgb = c2wxc.Get()

    diff = map(lambda x,y: y-x, c1rgb, c2rgb)

    rational_diff = [x * float(ratio) for x in diff]

    resulting_color = tuple(map(lambda x,y: int(round(x+y)), c1rgb, rational_diff))

    return wx.Colour(resulting_color).GetRGB()

def sgn(number):
    "sgn(number) - Returns the sign of the given _number_"
    try:
        if number > 0:
            return 1
        elif number == 0:
            return 0
        elif number < 0:
            return -1
    except:
        return None
