import unittest
import wx

class Fenestrum(wx.Frame):
  def __init__(ego):
    wx.Frame.__init__(ego, parent=None)
    ego.Bind(wx.EVT_PAINT, ego.CumIlluminatione)
    #ego.Bind(wx.EVT_CLOSE, ego.CumConclusione)
    
    w, h = ego.Size.Get()
    bmp = ego.bmp = wx.Bitmap(w, h)
    mdc = ego.mdc = wx.MemoryDC(bmp)
  
  def CumIlluminatione(ego, evt):
    #if not hasattr(ego, 'x'):
    #  print [i for i in dir(wx) if i.find('DC') > -1]
    #  ego.x = 0
    dc = wx.PaintDC(ego)
    mdc = ego.mdc; bmp = ego.bmp
    w, h = ego.ClientSize.Get()
    #bmp.SetSize(ego.ClientSize)
    ow, oh = bmp.Size.Get()
    if ow < w or oh < h:
      nbmp = wx.Bitmap(w, h)
      nmdc = wx.MemoryDC(nbmp)
      nmdc.Blit(0, 0, ow, oh, mdc, 0, 0)
      bmp = ego.bmp = nbmp
      mdc.SelectObject(bmp)
      mdc.Clear()
    dc.Blit(0, 0, w, h, mdc, 0, 0)
    evt.Skip()
      
  #def CumConclusione(ego, evt):
  #  evt.Skip()
    
class App(wx.App):
  def __init__(ego):
    wx.App.__init__(ego, redirect=False)
    IdFenestrum = Fenestrum()
    IdFenestrum.Show()
    ego.SetTopWindow(IdFenestrum)

class TestGraphics(unittest.TestCase):

    def test_graphics(self):
      app = App()
      app.MainLoop()

if __name__ == '__main__':
    unittest.main()