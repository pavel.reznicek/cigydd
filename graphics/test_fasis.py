#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import wx
import cigydd.graphics.fasis as fasis

class TestFasis(unittest.TestCase):

    def test_fasis(self):
        app = wx.App()
        colour = fasis.fasis(0.5, scale=(0x000000, 0xffffff), step=1, flags=fasis.FAS_NOTHING)
        right_colour = wx.Colour(0x80, 0x80, 0x80).RGB
        print("test gray colour:", hex(colour))
        print("should be:       ", hex(right_colour))
        self.assertEqual(colour, right_colour)

    def test_intercolour(self):
        black = 0x000000
        white = 0xffffff
        app = wx.App()
        gray = fasis.intercolour(black, white, 0.5)
        right_gray = 0x808080
        self.assertEqual(gray, right_gray)

if __name__ == '__main__':
    unittest.main()