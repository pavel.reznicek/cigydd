#!/usr/bin/env python

import wx
import wx.media
import os

class MediaFrame(wx.Frame):

    def __init__(self, parent=None):
        wx.Frame.__init__(self, parent)
        self.button = wx.Button(self, -1, "Play!") 

        module_dir = os.path.dirname(__file__)
        self.soundfile = os.path.join(module_dir, "pyp", "pyp.wav")
        if 'wxMSW' in wx.PlatformInfo:
            backend = wx.media.MEDIABACKEND_WMP10
        elif 'wxMac' in wx.PlatformInfo:
            backend = wx.media.MEDIABACKEND_QUICKTIME
        elif 'wxGTK' in wx.PlatformInfo:
            backend = wx.media.MEDIABACKEND_GSTREAMER

        try:
            self.mediaPlayer = wx.media.MediaCtrl(self, -1, szBackend = backend)
        except NotImplementedError:
            raise
        
        self.Bind(wx.EVT_BUTTON, self.onClick, self.button)
        self.Bind(wx.media.EVT_MEDIA_LOADED, self.onMediaLoaded)

    def onClick(self, event):
        print("soundfile is %s" % self.soundfile)
        ret1 = self.mediaPlayer.Load(self.soundfile)
#        ret2 = self.mediaPlayer.LoadFromURI(self.soundfile)
#        ret3 = self.mediaPlayer.LoadURI(self.soundfile)
        print("Load:  ", ret1)
#        print "LoadFromURI:  ", ret2
#        print "LoadURI:  ", ret3

    def onMediaLoaded(self, event):
        print("I am here.")
        self.mediaPlayer.Play()
        print("Did you hear anything?")

if __name__ == "__main__":
    app = wx.App(False)
    frame = MediaFrame()
    frame.Show()
    app.MainLoop()
