# cigydd
Cigydd’s Python Library

A small set of tiny packages with various Python utilities.
Required for some of my Python programs.

Installation
============
Place the package directory somewhere in your Python path, e. g. /usr/lib/python2.7/dist-packages.