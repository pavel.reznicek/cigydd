#!/usr/bin/env python
# -*- coding: utf-8 -*-

def bin(x):
  vysl = ''
  if type(x) in (str, unicode):
    x = int(x) # případně vyhodí chybu
  if type(x) in (int, long, bint, blong, float):
    while x:
      vysl = str(x % 2) + vysl
      x /= 2
  if vysl == '': vysl = 0
  return vysl
  
def from_bin(s):
  vysl = 0
  for i in range(len(s)):
    if s[i] == '1':
      vysl |= 2 ** (len(s) - 1 - i)
  return vysl
  
def _representation(x):
    return '%s(%s)' % (
      x.__class__.__name__,
      x.__class__.__base__.__repr__(x)
      )

class BinaryConverterMixin(object):
  def get_bin(self):
    return bin(self)
  bin = property(get_bin)
  def from_bin(self, s):
    return self.__class__(from_bin(s))

class bint(int, BinaryConverterMixin):
  def __repr__(self):
    return _representation(self)
