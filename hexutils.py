#!/usr/bin/env python

import os
import pyperclip

def hex2str(digits):
    '''
    Returns a string build from a chain of sedecimal digits.

    digits:
        the input sedecimal run.
        Can contain any garbage such as whitespace
        as only the sedecimal digits are taken into account.
    '''
    raw_digits = [d for d in digits if d in '0123456789ABCDEFabcdef']
    byte_list = []
    for i in range(0, len(raw_digits), 2):
        byte_digits = raw_digits[i:i + 2]
        byte = eval('0x' + ''.join(byte_digits))
        byte_list.append(byte)
    result = ''
    for j in byte_list:
        char = chr(j)
        result += char
    return result

def str2hex(s, asm=False):
    '''
    Builds a sedecimal run from the input string.

    s:
        intup string
    asm:
        assembler output
        (each byte prepended with a $ sign
        and bytes separated by commas)
    '''
    hbyte_list = []
    for c in s:
        if isinstance(c, int):
            byte = c
        elif isinstance(c, str):
            byte = ord(c)
        hbyte = hex(byte).upper()[2:].rjust(2,'0')
        if asm: hbyte = '$' + hbyte
        hbyte_list.append(hbyte)
    glue = ' ' if not asm else ','
    result = glue.join(hbyte_list)
    return result

def hex2dcb(s, add_comment=True):
    '''
    Returns a m68k assembler dc.b line built from a sedecimal run.

    s: 
        input sedecimal run
    add_comment: 
        adds a comment 
        containing the actual string
        (formatted as a Python representation;
        well, it's a bit provisory 
        but it's just a comment 😉)
    '''
    raw_data = hex2str(s)
    hbytes = str2hex(raw_data, True)
    if add_comment:
        space_count = 49 - len(hbytes)
        if space_count < 1:
            space_count = 1
        spaces = ' ' * space_count
        result = f"dc.b {hbytes}{spaces}// {repr(raw_data)}"
    else:
        result = f"dc.b {hbytes}"
    return result

def copyhex2dcb(s, add_comment=True):
    '''
    Builds an m68k assembler dc.b line from a sedecimal run, 
    prints it out, 
    and copies it to the system clipboard.

    s: 
        input sedecimal run
    add_comment: 
        adds a comment 
        containing the actual string
        (formatted as a Python representation;
        well, it's a bit provisory 
        but it's just a comment 😉)
    '''
    dcb = hex2dcb(s, add_comment=add_comment)
    print(dcb)
    pyperclip.copy(dcb)

def copystr2dcb(s, add_comment=True):
    '''
    Builds an m68k assembler dc.b line from a string, 
    prints it out, 
    and copies it to the system clipboard.

    s: 
        input string
    add_comment: 
        adds a comment 
        containing the actual string
        (formatted as a Python representation;
        well, it's a bit provisory 
        but it's just a comment 😉)
    '''
    # the string to a sedecimal run
    hbytes = str2hex(s)
    # the sedecimal run to a dc.b line
    dcb = hex2dcb(hbytes, add_comment=add_comment)
    print(dcb)
    pyperclip.copy(dcb)

def make_zeroes_line(count):
    """
    Builds up a dc.b line of <count> zero bytes
    """
    dcb = 'dc.b'
    zeroes = ['$00'] * count
    sedrun = ','.join(zeroes)
    line = f'{dcb}  {sedrun}{os.linesep}'
    return line

def makebss(size):
    step = 16
    result = ''
    for i in range(0, size, step):
        line = make_zeroes_line(step)
        result += line
    rest = size % step
    if rest > 0:
        line = make_zeroes_line(rest)
        result += line
    return result

def copybss(size):
    bss = makebss(size)
    print(bss)
    pyperclip.copy(bss)

def incbin(filename, offset, length=-1):
    with open(filename, 'rb') as binfile:
        binfile.seek(offset)
        binary = binfile.read(length)
    linelength = 16
    dcb = 'dc.b'
    indent = ' ' * 2
    result = ''
    for b in range(0, len(binary), linelength):
        binline = binary[b:b + linelength]
        sedrun = str2hex(binline, asm=True)
        comment = f';H{hex(offset + b)[2:].upper()}{os.linesep}'
        result += comment
        line = f'{indent}{dcb}{indent}{sedrun}{os.linesep}'
        result += line
    return result

def copy_binary_to_include(filename, offset, length=-1):
    included_binary = incbin(filename, offset, length)
    print(included_binary)
    pyperclip.copy(included_binary)
        
    
    
